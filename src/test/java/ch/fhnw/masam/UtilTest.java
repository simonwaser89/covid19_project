package ch.fhnw.masam;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class UtilTest {

	@Test
	public void calculationTest() {
		int population = 100;
		int cases = 10;
		assertEquals(Util.getPercentage(population, cases), 10);
	}
	
}
