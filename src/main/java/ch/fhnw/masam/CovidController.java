package ch.fhnw.masam;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class defines the REST operations.
 * @author David Herzig
 *
 */
@RestController
@RequestMapping("covidservice")
public class CovidController {

	@Autowired
	private CovidService covidService;
	
	private static final Logger LOG = LogManager.getLogger(CovidController.class);
	
	/**
	 * Query by a specific date.
	 * @param date date.
	 * @return List of all cases world wide for the given date.
	 */
	@GetMapping("/bydate")
	public List<CovidCase> getByDate(@RequestParam(value = "date") String date) {
		LOG.info("query by date " + date);
		List<CovidCase> cases = covidService.findByDate(date);
		return cases;
	}
	
	@GetMapping("/bycountry")
	public List<CovidCase> getByCountry(@RequestParam(value = "country") String country) {
		LOG.info("query by country " + country);
		List<CovidCase> cases = covidService.findByCountry(country);
		return cases;
	}
	
	@GetMapping("/allcountries")
	public List<String> getAllCountries() {
		LOG.info("query all countries");
		List<String> result = covidService.getAllCountries();
		return result;
	}
	
}
