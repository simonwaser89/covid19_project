package ch.fhnw.masam.dataloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.opencsv.CSVReader;

public class Loader_V2 {

	public static void main(String [] args) {
		
		// check command line parameter
		if (args.length != 1) {
			System.err.println("Usage: java -jar loader.jar app.properties");
			System.exit(0);
		}
		
		String propertyFile = args[0];
		System.out.println("Property File: " + propertyFile);
		
		// validate file
		File file = new File(propertyFile);
		if (!file.exists()) {
			System.err.println("File " + propertyFile + " not found");
			System.exit(0);
		}
		
		// open property file and read values
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(file));
			// read parameters
			String geneInfoFile = properties.getProperty("dataFile");
			String hostname = properties.getProperty("hostname");
			Long port = Long.parseLong(properties.getProperty("port"));
			String dbname = properties.getProperty("dbname");
			String username = properties.getProperty("username");
			String password = properties.getProperty("password");
			
			Long start = System.currentTimeMillis();
			loadData(geneInfoFile, hostname, port, dbname, username, password);
			Long stop = System.currentTimeMillis();
			
			System.out.println("time consumed " + (stop-start)/1000.0f);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(0);
		}
	}
	
	private static void loadData(String dataFile, String hostname, Long port, String dbname, String username, String password) throws Exception {
		//open db connection
		String connStr = "jdbc:mysql://" + hostname + ":" + port + "/" + dbname;
		Connection connection = DriverManager.getConnection(connStr, username, password);
		String insertCmd = createInsertCommand(connection);
		
		int counter = 0;
		final int CHUNK_SIZE = 50000;
		List<String> rowsToInsert = new ArrayList();
		
		try (CSVReader reader = new CSVReader(new FileReader(dataFile))) {
			reader.readNext();
			String [] row = null;
			while ((row = reader.readNext()) != null) {
				rowsToInsert.add(createParameterString(row, connection, insertCmd));
				//writeRow(row, connection, insertCmd);
				counter++;
				if (counter % CHUNK_SIZE == 0) {
					executeInsert(rowsToInsert, connection, insertCmd);
					rowsToInsert.clear();
				}
			}
			if (rowsToInsert.size() > 0) {
				executeInsert(rowsToInsert, connection, insertCmd);
			}
		}
		
		System.out.println(counter);
		connection.close();
	}
	
	private static String createParameterString(String [] row, Connection connection, String insertCmd) throws Exception {
		PreparedStatement stmt = connection.prepareStatement(insertCmd);
		
		for (int i=0; i<row.length; i++) {
			if (row[i].trim().equals("-")) {
				stmt.setNull(i+1, Types.VARCHAR);
			} else {
				stmt.setString(i+1, row[i]);
			}
		}
		
		// handle integers
		if (row[1].trim().equals("")) stmt.setNull(2, Types.INTEGER);
		else stmt.setInt(2, Integer.parseInt(row[1]));
		
		if (row[2].trim().equals("")) stmt.setNull(3, Types.INTEGER);
		else stmt.setInt(3, Integer.parseInt(row[2]));
		
		if (row[3].trim().equals("")) stmt.setNull(4, Types.INTEGER);
		else stmt.setInt(4, Integer.parseInt(row[3]));
		
		if (row[14].trim().equals("")) stmt.setNull(15, Types.INTEGER);
		else stmt.setInt(15, Integer.parseInt(row[14]));
		
		if (row[15].trim().equals("")) stmt.setNull(16, Types.INTEGER);
		else stmt.setInt(16, Integer.parseInt(row[15]));
		
		// handle date
		if (row[4].trim().equals("")) stmt.setNull(5, Types.DATE);
		else {
			java.util.Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(row[4]);
			Date date = new Date(utilDate.getTime());
			stmt.setDate(5, date);
		}
		
		// handle floats
		if (row[12].trim().equals("")) stmt.setNull(13, Types.FLOAT);
		else stmt.setFloat(13, Float.parseFloat(row[12]));
		if (row[13].trim().equals("")) stmt.setNull(14, Types.FLOAT);
		else stmt.setFloat(14, Float.parseFloat(row[13]));
		
		String stmtStr = stmt.toString();
		int startIndex = stmtStr.substring(stmtStr.indexOf("VALUES")).indexOf("(");
		stmt.close();
		String result = stmtStr.substring(stmtStr.indexOf("VALUES")).substring(startIndex);
		return result;
	}
	
	
	private static String createInsertCommand(Connection connection) throws Exception {
		ResultSet rs = connection.getMetaData().getColumns(null, null, "tcovid_cases", null);
		String cmd = "insert into tcovid_cases(";
		String parameters = "(";
		
		while (rs.next()) {
			cmd += rs.getString("COLUMN_NAME") + ",";
			parameters += "?,";
		}
		
		cmd = cmd.substring(0, cmd.length() - 1) + ")";
		parameters = parameters.substring(0, parameters.length() - 1) + ")";
		
		return cmd + " VALUES " + parameters;
	}
	
	private static void executeInsert(List<String> rowsToInsert, Connection connection, String insertCmd) throws Exception {
		System.out.println("insert chunk with size: " + rowsToInsert.size());
		String newICmd = insertCmd.substring(0, insertCmd.indexOf(")") + 1) + " VALUES ";
		StringBuilder sb = new StringBuilder();
		for (String row : rowsToInsert) {
			sb.append(row + ",");
		}
		
		newICmd = newICmd + sb.toString();
		newICmd = newICmd.substring(0, newICmd.length() - 1);
		
		PreparedStatement stmt = connection.prepareStatement(newICmd);
		stmt.executeUpdate();
		stmt.close();
	}
}

